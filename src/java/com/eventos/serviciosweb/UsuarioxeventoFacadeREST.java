/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eventos.serviciosweb;


import com.eventos.jpa.Usuario;
import com.eventos.jpa.Evento;
import com.eventos.jpa.Usuarioxevento;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;


/**
 *
 * @author Driss
 */
@Stateless
@Path("com.eventos.jpa.usuarioxevento")
public class UsuarioxeventoFacadeREST extends AbstractFacade<Usuarioxevento> {
    @EJB
    private EventoFacadeREST eventoFacadeREST;
    @EJB
    private UsuarioFacadeREST usuarioFacadeREST;
    
    @PersistenceContext(unitName = "ServidorUMAEventsPU")
    private EntityManager em;
    
   
    
    
    public UsuarioxeventoFacadeREST() {
        super(Usuarioxevento.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Usuarioxevento entity) {
        super.create(entity);
    }

    @POST
    @Path("/asistir")
    @Consumes({"application/xml", "application/json"})
    public void asistirAEvento(@QueryParam("evento") BigDecimal idevento, @QueryParam("token") String token) {
        Usuario u = usuarioFacadeREST.findByToken(token);
       
       
        if(null!=u){
            Evento e = eventoFacadeREST.find(idevento);
            Usuarioxevento uxe = new Usuarioxevento();
            uxe.setIdusuario(u);
            uxe.setIdevento(e);
            Integer plazas = e.getPlazas()-1;
            e.setPlazas(plazas);
            eventoFacadeREST.edit(e);
            create(uxe);
            
        }
        System.out.println("ok"+idevento+"   "+token);
    }
    
    @GET
    @Path("/attendance")
    @Consumes({"application/xml", "application/json"})
    public Boolean asiste(@QueryParam("idevento") BigDecimal idevento, @QueryParam("token") String token) {
        Usuario u = usuarioFacadeREST.findByToken(token);
        
        Evento e = eventoFacadeREST.find(idevento);
        List <Usuarioxevento> lista = u.getUsuarioxeventoCollection();
       List <Evento> listaevento = new ArrayList<>();
            for (Usuarioxevento usuarioxevento : lista) {
              listaevento.add(usuarioxevento.getIdevento());
            }
            
            return listaevento.contains(e);

    }
    
    
    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") BigDecimal id, Usuarioxevento entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") BigDecimal id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Usuarioxevento find(@PathParam("id") BigDecimal id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Usuarioxevento> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<Usuarioxevento> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
