/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eventos.serviciosweb;

import com.eventos.jpa.Evento;
import com.eventos.jpa.Sede;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


/**
 *
 * @author Driss
 */
@Stateless
@Path("com.eventos.jpa.evento")
public class EventoFacadeREST extends AbstractFacade<Evento> {
    @EJB
    private SedeFacadeREST sedeFacadeREST;
    @PersistenceContext(unitName = "ServidorUMAEventsPU")
    private EntityManager em;

    
    
    public EventoFacadeREST() {
        super(Evento.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Evento entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") BigDecimal id, Evento entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") BigDecimal id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Evento find(@PathParam("id") BigDecimal id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Evento> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<Evento> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @GET
    @Path("/tipo/{tipo}")
    @Produces({"application/json"})
    public List<Evento> findEventosbyTipo(@PathParam("tipo") String tipo){
    
        return em.createNamedQuery("Evento.findByTipoevento").setParameter("tipoevento", tipo).getResultList();
    }
    
    @GET
    @Path("/sede/{sede}")
    @Produces({"application/json"})
    public List<Evento> findEventosbyTipo(@PathParam("sede") Short idsede){
        Sede sede = sedeFacadeREST.find(idsede);
        return sede.getEventoList();
       // return em.createNamedQuery("Evento.findByTipoevento").setParameter("tipoevento", tipo).getResultList();
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
