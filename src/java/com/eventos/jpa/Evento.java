/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eventos.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Driss
 */
@Entity
@Table(name = "EVENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evento.findAll", query = "SELECT e FROM Evento e"),
    @NamedQuery(name = "Evento.findByIdevento", query = "SELECT e FROM Evento e WHERE e.idevento = :idevento"),
    @NamedQuery(name = "Evento.findByTitulo", query = "SELECT e FROM Evento e WHERE e.titulo = :titulo"),
    @NamedQuery(name = "Evento.findByDescripcion", query = "SELECT e FROM Evento e WHERE e.descripcion = :descripcion"),
    @NamedQuery(name = "Evento.findByFechacomienzo", query = "SELECT e FROM Evento e WHERE e.fechacomienzo = :fechacomienzo"),
    @NamedQuery(name = "Evento.findByFechafinal", query = "SELECT e FROM Evento e WHERE e.fechafinal = :fechafinal"),
    @NamedQuery(name = "Evento.findByTipoevento", query = "SELECT e FROM Evento e WHERE e.tipoevento = :tipoevento"),
    @NamedQuery(name = "Evento.findByPlazas", query = "SELECT e FROM Evento e WHERE e.plazas = :plazas")})
public class Evento implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evento_seq")
    @SequenceGenerator(name = "evento_seq", sequenceName = "evento_seq", initialValue = 15)
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDEVENTO")
    private BigDecimal idevento;
    @Size(max = 100)
    @Column(name = "TITULO")
    private String titulo;
    @Size(max = 250)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "FECHACOMIENZO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacomienzo;
    @Column(name = "FECHAFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechafinal;
    @Size(max = 100)
    @Column(name = "TIPOEVENTO")
    private String tipoevento;
    @Column(name = "PLAZAS")
    private Integer plazas;
    @JoinColumn(name = "IDSEDE", referencedColumnName = "IDSEDE")
    @ManyToOne
    private Sede idsede;
    @OneToMany(mappedBy = "idevento")
    private List<Usuarioxevento> usuarioxeventoList;

    public Evento() {
    }

    public Evento(BigDecimal idevento) {
        this.idevento = idevento;
    }

    public BigDecimal getIdevento() {
        return idevento;
    }

    public void setIdevento(BigDecimal idevento) {
        this.idevento = idevento;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechacomienzo() {
        return fechacomienzo;
    }

    public void setFechacomienzo(Date fechacomienzo) {
        this.fechacomienzo = fechacomienzo;
    }

    public Date getFechafinal() {
        return fechafinal;
    }

    public void setFechafinal(Date fechafinal) {
        this.fechafinal = fechafinal;
    }

    public String getTipoevento() {
        return tipoevento;
    }

    public void setTipoevento(String tipoevento) {
        this.tipoevento = tipoevento;
    }

    public Integer getPlazas() {
        return plazas;
    }

    public void setPlazas(Integer plazas) {
        this.plazas = plazas;
    }

    public Sede getIdsede() {
        return idsede;
    }

    public void setIdsede(Sede idsede) {
        this.idsede = idsede;
    }

    @XmlTransient
    public List<Usuarioxevento> getUsuarioxeventoList() {
        return usuarioxeventoList;
    }

    public void setUsuarioxeventoList(List<Usuarioxevento> usuarioxeventoList) {
        this.usuarioxeventoList = usuarioxeventoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idevento != null ? idevento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evento)) {
            return false;
        }
        Evento other = (Evento) object;
        if ((this.idevento == null && other.idevento != null) || (this.idevento != null && !this.idevento.equals(other.idevento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.eventos.jpa.Evento[ idevento=" + idevento + " ]";
    }
    
}
