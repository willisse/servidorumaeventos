/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eventos.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Driss
 */
@Entity
@Table(name = "USUARIOXEVENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuarioxevento.findAll", query = "SELECT u FROM Usuarioxevento u"),
    @NamedQuery(name = "Usuarioxevento.findByIdusuarioxevento", query = "SELECT u FROM Usuarioxevento u WHERE u.idusuarioxevento = :idusuarioxevento")})
public class Usuarioxevento implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuarioxevento_seq")
    @SequenceGenerator(name = "usuarioxevento_seq", sequenceName = "usuarioxevento_seq", initialValue = 15)
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDUSUARIOXEVENTO")
    private BigDecimal idusuarioxevento;
    @JoinColumn(name = "IDUSUARIO", referencedColumnName = "IDUSUARIO")
    @ManyToOne
    private Usuario idusuario;
    @JoinColumn(name = "IDEVENTO", referencedColumnName = "IDEVENTO")
    @ManyToOne
    private Evento idevento;

    public Usuarioxevento() {
    }

    public Usuarioxevento(BigDecimal idusuarioxevento) {
        this.idusuarioxevento = idusuarioxevento;
    }

    public BigDecimal getIdusuarioxevento() {
        return idusuarioxevento;
    }

    public void setIdusuarioxevento(BigDecimal idusuarioxevento) {
        this.idusuarioxevento = idusuarioxevento;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    public Evento getIdevento() {
        return idevento;
    }

    public void setIdevento(Evento idevento) {
        this.idevento = idevento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idusuarioxevento != null ? idusuarioxevento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuarioxevento)) {
            return false;
        }
        Usuarioxevento other = (Usuarioxevento) object;
        if ((this.idusuarioxevento == null && other.idusuarioxevento != null) || (this.idusuarioxevento != null && !this.idusuarioxevento.equals(other.idusuarioxevento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.eventos.jpa.Usuarioxevento[ idusuarioxevento=" + idusuarioxevento + " ]";
    }
    
}
