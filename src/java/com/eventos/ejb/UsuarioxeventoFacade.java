/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eventos.ejb;

import com.eventos.jpa.Usuarioxevento;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Driss
 */
@Stateless
public class UsuarioxeventoFacade extends AbstractFacade<Usuarioxevento> {
    @PersistenceContext(unitName = "ServidorUMAEventsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioxeventoFacade() {
        super(Usuarioxevento.class);
    }
    
}
